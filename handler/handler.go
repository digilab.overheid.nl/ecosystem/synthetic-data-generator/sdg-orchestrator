package handler

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator/application"
)

type Handler struct {
	http.Server
}

func New(app *application.Application, listenAddress string) *Handler {
	return &Handler{
		Server: http.Server{
			Addr:    listenAddress,
			Handler: handler(app),
		},
	}
}

func handler(app *application.Application) http.Handler {
	router := chi.NewRouter()
	router.Use(middleware.Logger)
	router.Use(middleware.SetHeader("Content-Type", "application/json"))
	router.Get("/status", app.GetStatus)
	router.Post("/seed", app.StartSeeders)
	router.Delete("/reset", app.Reset)
	router.Post("/start", app.Start)

	return router
}
