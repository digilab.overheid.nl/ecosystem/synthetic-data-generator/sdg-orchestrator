module gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator

go 1.22

require (
	github.com/alecthomas/kong v0.9.0
	github.com/alecthomas/kong-yaml v0.2.0
	github.com/go-chi/chi/v5 v5.0.10
	github.com/nats-io/nats.go v1.31.0
)

require (
	github.com/klauspost/compress v1.17.0 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/nats-io/nkeys v0.4.5 // indirect
	github.com/nats-io/nuid v1.0.1 // indirect
	golang.org/x/crypto v0.14.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
