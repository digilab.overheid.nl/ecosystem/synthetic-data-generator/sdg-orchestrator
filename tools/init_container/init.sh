#!/usr/bin/env sh

set -e

NATS_EVENT_SUBJECT_NAMESPACE=events
NATS_TIME_SUBJECT_NAMESPACE=time
NATS_COMMAND_SUBJECT_NAMESPACE=command
SDG_NATS_SERVICE_HOST=${SDG_NATS_SERVICE_HOST:-host.docker.internal}
SDG_NATS_SERVICE_PORT=${SDG_NATS_SERVICE_PORT:-4222}
SDG_NATS_REPLICAS=${SDG_NATS_REPLICAS:-2}

printf 'Initializing NATS CLI to use %s:%d\n' "$SDG_NATS_SERVICE_HOST" "$SDG_NATS_SERVICE_PORT"

echo 'Waiting for NATS to become ready...'

until printf "." && nc -z -w 2 "$SDG_NATS_SERVICE_HOST" "$SDG_NATS_SERVICE_PORT"; do
    sleep 1;
done;

echo 'NATS OK ✓'

nats context add nats --server "$SDG_NATS_SERVICE_HOST:$SDG_NATS_SERVICE_PORT" --select

create_events_stream() {
    nats stream add --subjects="$NATS_EVENT_SUBJECT_NAMESPACE.>" --storage=memory --replicas="$SDG_NATS_REPLICAS" --retention=limits --discard=new --max-msgs=-1 --max-msgs-per-subject=-1 --max-bytes=128MB --max-age=1M --max-msg-size=1MB --dupe-window=1m --no-allow-rollup --deny-delete --no-deny-purge "$NATS_EVENT_SUBJECT_NAMESPACE"
}
create_time_stream() {
    nats stream add --subjects="$NATS_TIME_SUBJECT_NAMESPACE"    --storage=memory --replicas="$SDG_NATS_REPLICAS" --retention=limits --discard=old --max-msgs=8 --max-msgs-per-subject=-1 --max-bytes=1KB --max-age=1M --max-msg-size=32 --dupe-window=1m --no-allow-rollup --no-deny-delete --no-deny-purge "$NATS_TIME_SUBJECT_NAMESPACE"
}
create_command_stream() {
    nats stream add --subjects="$NATS_COMMAND_SUBJECT_NAMESPACE"    --storage=memory --replicas="$SDG_NATS_REPLICAS" --retention=limits --discard=old --max-msgs=8 --max-msgs-per-subject=-1 --max-bytes=1KB --max-age=1M --max-msg-size=32 --dupe-window=1m --no-allow-rollup --no-deny-delete --no-deny-purge "$NATS_COMMAND_SUBJECT_NAMESPACE"
}

if create_events_stream; then
    # Stream creation is idempotent, purge to make sure it's empty
    nats stream purge "$NATS_EVENT_SUBJECT_NAMESPACE" --force
else
    # Probable error: could not create Stream: stream name already in use with a different configuration (10058)
    nats stream rm "$NATS_EVENT_SUBJECT_NAMESPACE" --force
    create_events_stream
fi
if create_time_stream; then
    # Stream creation is idempotent, purge to make sure it's empty
    nats stream purge "$NATS_TIME_SUBJECT_NAMESPACE" --force
else
    # Probable error: could not create Stream: stream name already in use with a different configuration (10058)
    nats stream rm "$NATS_TIME_SUBJECT_NAMESPACE" --force
    create_time_stream
fi
if create_command_stream; then
    # Stream creation is idempotent, purge to make sure it's empty
    nats stream purge "$NATS_COMMAND_SUBJECT_NAMESPACE" --force
else
    # Probable error: could not create Stream: stream name already in use with a different configuration (10058)
    nats stream rm "$NATS_COMMAND_SUBJECT_NAMESPACE" --force
    create_command_stream
fi

printf 'NATS initialization done\n'
