package application

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"time"

	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator/simtimer"
)

type Application struct {
	logger  *slog.Logger
	client  *http.Client
	st      *simtimer.SimTimer
	seeders []string
}

func New(logger *slog.Logger, st *simtimer.SimTimer, seeders []string) *Application {
	return &Application{
		logger:  logger,
		client:  http.DefaultClient,
		st:      st,
		seeders: seeders,
	}
}

func (app *Application) setError(w http.ResponseWriter, err error) {
	app.logger.Error("request failed", "err", err)
	http.Error(w, err.Error(), http.StatusInternalServerError)
}

func (app *Application) GetStatus(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	status, err := app.st.Status(ctx)
	if err != nil {
		app.setError(w, fmt.Errorf("simtimer status failed: %w", err))
		return
	}

	if !r.URL.Query().Has(`debug`) {
		for _, consStat := range status.Consumers {
			for _, stream := range consStat {
				stream.JetStreamConsumerInfo = nil
			}
		}
	}

	if err := json.NewEncoder(w).Encode(status); err != nil {
		app.setError(w, fmt.Errorf("json encoding failed: %w", err))
		return
	}
}

func (app *Application) StartSeeders(w http.ResponseWriter, r *http.Request) {
	var body struct {
		Seed              *string `json:"seed,omitempty"`
		NumberOfInstances int     `json:"num_instances"` // Note: should match the json attribute used in the seeder, since the body is encoded below
	}

	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		app.setError(w, fmt.Errorf("json decoding failed: %w", err))
		return
	}

	b := new(bytes.Buffer)

	if err := json.NewEncoder(b).Encode(body); err != nil {
		app.setError(w, fmt.Errorf("json encoding failed: %w", err))
		return
	}

	for _, seeder := range app.seeders {
		req, err := http.NewRequestWithContext(r.Context(), http.MethodPost, seeder, b)
		if err != nil {
			app.setError(w, fmt.Errorf("new request with context failed: %w", err))
			return
		}

		req.Header.Add("Content-Type", "application/json")

		resp, err := app.client.Do(req)
		if err != nil {
			app.setError(w, fmt.Errorf("client do failed: %w", err))
			return
		}

		if resp.StatusCode != http.StatusOK {
			d, err := io.ReadAll(resp.Body)
			if err != nil {
				app.setError(w, fmt.Errorf("read body failed: %w", err))
				return
			}

			app.setError(w, fmt.Errorf("client do successful but wrong response: %s: %w", string(d), err))
			return
		}
	}

	w.WriteHeader(http.StatusOK)
}

func (app *Application) Reset(w http.ResponseWriter, r *http.Request) {
	// Stop the timer
	// Send message into nats to let downstream know we do a reset

	if app.st.IsRunning(r.Context()) {
		app.st.Stop(r.Context())
	}

	if err := app.st.ClearTimeStream(r.Context()); err != nil {
		app.setError(w, fmt.Errorf("clear time stream: %w", err))
		return
	}

	if err := app.st.ClearEventsStream(r.Context()); err != nil {
		app.setError(w, fmt.Errorf("clear events stream: %w", err))
		return
	}

	if err := app.st.PublishReset(r.Context()); err != nil {
		app.setError(w, fmt.Errorf("publish reset: %w", err))
		return
	}

	app.st.CurrentTime.Store(app.st.StartTime)

	w.WriteHeader(http.StatusOK)
}

func (app *Application) Start(w http.ResponseWriter, r *http.Request) {
	var body struct {
		StartTime time.Time `json:"startTime"`
		EndTime   time.Time `json:"endTime"`
	}

	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		app.setError(w, fmt.Errorf("decoding: %w", err))
		return
	}

	go app.st.Start(context.Background(), simtimer.Start{
		StartTime: body.StartTime,
		EndTime:   body.EndTime,
	})

	if err := app.st.PublishStart(r.Context()); err != nil {
		app.setError(w, fmt.Errorf("publish start: %w", err))
		return
	}

	w.WriteHeader(http.StatusOK)
}
