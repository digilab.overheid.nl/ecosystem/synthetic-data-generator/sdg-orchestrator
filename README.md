# SDG Orchestrator

This project is responsible for keeping the SDG Event Simulators in sync.
It does this by publishing timestamps to a stream, at a configurable rate, and monitoring if all consumers have acked the timestamp message.

It also initializes the required NATS JetStream streams.

## Overview

TODO: Project overview.


## Running the project

The slow-but-should-always-work one-liner is (assuming Docker Desktop, and a running NATS server or a portforwarding to it on the docker host):
```sh
docker run --rm \
    --env SDG_NATS_SERVICE_HOST=host.docker.internal \
    --env SDG_NATS_SERVICE_PORT=4222 \
    --env SDG_SIMULATION_END=1991-01-01T00:00:00.00Z \
    --env SDG_SIMULATION_START=1990-01-01T00:00:00.00Z \
    --env SDG_SIMULATION_TIME_CONSUMER_NAMES=sdg-event-simulator-person \
    --env SDG_SIMULATION_TIME_STEP=6h \
    --env SERVER_PORT=8080 \
    --publish 8082:8080 \
    $(docker build -q -f Dockerfile .)
```

API call to get the status of the orchestrator:
```sh
curl 'http://localhost:8082/status' | jq .
```

To include debug information (mostly NATS JetStream Consumer related):
```sh
curl 'http://localhost:8082/status?debug' | jq .
```

## Developer documentation

If you would like to contribute to this project, consult the [`CONTRIBUTING.md`](CONTRIBUTING.md) file.


## License

Copyright © VNG Realisatie 2023

[Licensed under the EUPLv1.2](LICENSE)

[You can find more information about the license here](https://commission.europa.eu/content/european-union-public-licence_en).
