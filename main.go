package main

import (
	"log"

	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator/cmd"
)

func main() {
	if err := cmd.Run(); err != nil {
		log.Fatal(err)
	}
}
