package cmd

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"time"

	_ "github.com/alecthomas/kong-yaml"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator/application"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator/handler"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator/process"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator/simtimer"
)

type ServeCmd struct {
	handler       *handler.Handler
	simtimer      *simtimer.SimTimer
	ListenAddress string `env:"APP_LISTEN_ADDRESS" default:"0.0.0.0:8080" name:"listen-address" help:"Address to listen  on."`
	NATS          struct {
		Host string `env:"SDG_NATS_SERVICE_HOST" name:"host" help:"NATS service host"`
		Port uint16 `env:"SDG_NATS_SERVICE_PORT" name:"port" help:"NATS service port"`
	} `embed:"" prefix:"nats-service-"`
	Simulation struct {
		Start                time.Time     `env:"SDG_SIMULATION_START" default:"1990-01-01T00:00:00.00Z" name:"start" help:"Simulation start"`
		End                  time.Time     `env:"SDG_SIMULATION_END" default:"1990-01-02T00:00:00.00Z" name:"end" help:"Simulation end"`
		ConsumerNames        []string      `env:"SDG_SIMULATION_CONSUMER_NAMES" name:"consumer-names" help:"Simulation consumer names in a comma separated list"`
		TimeStep             time.Duration `env:"SDG_SIMULATION_TIME_STEP" default:"6h" name:"time-step" help:"Simulation step"`
		ConsumerLagTolerance int           `env:"SDG_SIMULATION_CONSUMER_LAG_TOLERANCE" default:"2" name:"consumer-lag-tolerance" help:"Simulation consumer lag tolerance sets the number of nats messages a consumer is allowed to be behind, needs to be lower than the 'time' stream's max-msgs parameter"`
		Seeders              []string      `env:"SDG_SIMULATION_SEEDERS" name:"seeders" help:"Simulation seeders in a comma separated list"`
	} `embed:"" prefix:"simulation-"`
}

func (cmd *ServeCmd) Run(ctx *Context) error {
	proc := process.New()

	logger := ctx.Logger
	logger.Info("Starting orchestrator", "config", cmd)

	nc, js, err := newNatsJetStream(logger, cmd.NATS.Host, cmd.NATS.Port)
	if err != nil {
		return fmt.Errorf("nats jet stream failed: %w", err)
	}

	defer nc.Close()
	defer nc.Drain()

	options := []simtimer.Option{
		simtimer.WithStartTime(cmd.Simulation.Start),
		simtimer.WithEndTime(cmd.Simulation.End),
		simtimer.WithTimeStep(cmd.Simulation.TimeStep),
		simtimer.WithConsumerNames(cmd.Simulation.ConsumerNames),
		simtimer.WithConsumerLagTolerance(cmd.Simulation.ConsumerLagTolerance),
	}

	if cmd.simtimer, err = simtimer.New(logger, js, options...); err != nil {
		return fmt.Errorf("simtimer new failed: %w", err)
	}

	app := application.New(logger, cmd.simtimer, cmd.Simulation.Seeders)

	cmd.handler = handler.New(app, cmd.ListenAddress)

	go func() {
		logger.Info("Starting http server", "address", cmd.ListenAddress)

		if err := cmd.handler.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			logger.Error("listen and serve failed", "err", err)
		}

		// Make sure that the proc shuts down otherwise we stay alive without an http server
		proc.Shuwdown()
	}()

	go func() {
		logger.Info("Starting simtimer")

		cmd.simtimer.Start(context.Background(), simtimer.Start{
			StartTime: cmd.simtimer.StartTime,
			EndTime:   cmd.simtimer.EndTime,
		})
	}()

	proc.Wait()

	cmd.shutdown(logger)

	return nil
}

func (cmd *ServeCmd) shutdown(logger *slog.Logger) {
	logger.Info("shutting down server")

	// Shutdown application
	shutdownCtx := context.Background()

	cmd.handler.Shutdown(shutdownCtx)

	cmd.simtimer.Stop(shutdownCtx)

	logger.Info("shutdown finished")
}
