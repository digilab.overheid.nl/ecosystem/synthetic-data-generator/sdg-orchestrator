package cmd

import (
	"fmt"
	"log/slog"

	"github.com/nats-io/nats.go"
	"github.com/nats-io/nats.go/jetstream"
)

func newNatsJetStream(logger *slog.Logger, natsHost string, natsPort uint16) (*nats.Conn, jetstream.JetStream, error) {
	natsUrl := fmt.Sprintf(`nats://%s:%d`, natsHost, natsPort)

	logger.Debug("", "nats-url", natsUrl)

	nc, err := nats.Connect(natsUrl)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to connect to NATS: %w", err)
	}

	js, err := jetstream.New(nc)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to create NATSA JetStream instance: %w", err)
	}

	return nc, js, nil
}
