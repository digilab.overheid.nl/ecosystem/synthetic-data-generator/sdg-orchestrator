FROM golang:1.22.2-alpine3.18 AS builder

WORKDIR /build

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o server

## Run the server for dev
FROM digilabpublic.azurecr.io/alpine:3.19

EXPOSE 8080

COPY --from=builder /build/server .

CMD /server serve
