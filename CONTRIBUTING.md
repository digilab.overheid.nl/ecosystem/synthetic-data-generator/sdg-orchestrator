# Contributing


## Setup your development environment

### Requirements

* TODO: list required dependencies


## Running locally

(This requires the NATS JetStream instance is already set up, with a port mapping on localhost:4222)

Build and run:
```sh
make prod
```

Run for dev (using `go run`):
```sh
make dev
```
