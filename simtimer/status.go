package simtimer

import (
	"context"
	"errors"
	"time"

	"github.com/nats-io/nats.go/jetstream"
)

type status struct {
	// SimTimer config:
	StartTime            time.Time `json:"startTime"`
	EndTime              time.Time `json:"endTime"`
	TimeStep             string    `json:"timeStep"`
	ConsumerNames        []string  `json:"consumerNames"`
	ConsumerLagTolerance int       `json:"consumerLagTolerance"`
	// SimTimer status
	CurrentTime time.Time `json:"currentTime"`
	Count       int64     `json:"count"`
	// Consumer status:
	Consumers map[string]map[string]*consumerStatus `json:"consumers"`
	State     State                                 `json:"state"`
}

type consumerStatus struct {
	Error                 string                  `json:"error,omitempty"`
	PendingMessages       uint64                  `json:"pendingMessages"`
	Lagging               bool                    `json:"lagging"`
	JetStreamConsumerInfo *jetstream.ConsumerInfo `json:"jetStreamConsumerInfo,omitempty"`
}

func (st *SimTimer) Status(ctx context.Context) (*status, error) {
	consumers := map[string]map[string]*consumerStatus{}
	for _, consName := range st.ConsumerNames {
		consumers[consName] = make(map[string]*consumerStatus)

		for _, stream := range []string{NatsTimeStreamName, NatsCommandStreamName} {
			consStat := &consumerStatus{}
			consumers[consName][stream] = consStat

			cons, err := st.js.Consumer(ctx, stream, consName)
			if err != nil {
				if errors.Is(err, context.Canceled) {
					return nil, err
				}

				st.logger.Error("Failed to get info of consumer", "err", err)
				consStat.Error = err.Error()
				continue
			}

			consInfo, err := cons.Info(ctx)
			if err != nil {
				st.logger.Error("Failed to get info of consumer", "err", err)
				consStat.Error = err.Error()
				continue
			}

			consStat.PendingMessages = consInfo.NumPending
			if consInfo.NumPending > uint64(st.ConsumerLagTolerance) {
				consStat.Lagging = true
			}

			consStat.JetStreamConsumerInfo = consInfo
		}
	}

	return &status{
		StartTime:            st.StartTime,
		EndTime:              st.EndTime,
		TimeStep:             st.TimeStep.String(),
		ConsumerNames:        st.ConsumerNames,
		ConsumerLagTolerance: st.ConsumerLagTolerance,
		CurrentTime:          st.CurrentTime.Load().(time.Time),
		Count:                st.Count.Load(),
		Consumers:            consumers,
		State:                st.state,
	}, nil
}
