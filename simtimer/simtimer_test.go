package simtimer

import (
	"testing"
	"time"
)

func Test_makeTimeMessage(t *testing.T) {
	type args struct {
		t time.Time
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"zero",
			args{
				time.Time{},
			},
			"0001-01-01T00:00:00.00Z",
		},
		{
			"1983",
			args{
				time.Date(1984, 2, 3, 4, 5, 6, 990_000_000, time.UTC),
			},
			"1984-02-03T04:05:06.99Z",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := makeTimeMessage(tt.args.t); got != tt.want {
				t.Errorf("makeTimeMessage() = %v, want %v", got, tt.want)
			}
		})
	}
}
