package simtimer

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"strings"
	"sync/atomic"
	"time"

	"github.com/nats-io/nats.go/jetstream"
)

const WaitDelay = 10
const (
	NatsTimeStreamName     = `time`
	NatsTimeSubject        = `time`
	NatsEventsStreamName   = `events`
	NatsEventsSubject      = `events`
	NatsCommandStreamName  = `command`
	NatsCommandSubject     = `command`
	TimeFormat             = `2006-01-02T15:04:05.00Z`
	ConsumerWaitDelay      = WaitDelay * time.Millisecond // wait for this amount of time between re-checking if consumers are up-to-date
	ConsumerWaitLogCounter = 2000 / WaitDelay             // warning-log only after this number of ConsumerWaitDelay's
)

type State string

const (
	StateUnset    State = "unset"
	StateStarting State = "starting"
	StateRunning  State = "running"
	StateStopped  State = "stopped"
)

type SimTimer struct {
	StartTime            time.Time
	EndTime              time.Time
	TimeStep             time.Duration
	ConsumerNames        []string
	ConsumerLagTolerance int

	CurrentTime atomic.Value
	Count       atomic.Int64
	stop        chan bool

	state  State
	logger *slog.Logger

	js jetstream.JetStream
}

func New(logger *slog.Logger, js jetstream.JetStream, options ...Option) (*SimTimer, error) {
	st := &SimTimer{
		StartTime: time.Unix(0, 0),                // 1970-01-01
		EndTime:   time.Unix(0, 0).Add(1<<63 - 1), // 2262-04-11
		TimeStep:  24 * time.Hour,
		stop:      make(chan bool),
		js:        js,
		state:     StateStarting,
		logger:    logger.WithGroup("SimTimer"),
	}

	for _, opt := range options {
		opt(st)
	}

	currentTime, err := st.getCurrentTimeFromStream(st.StartTime.Add(-st.TimeStep))
	if err != nil {
		return nil, fmt.Errorf("failed to determine current time from stream: %w", err)
	}

	st.CurrentTime.Store(currentTime.Add(st.TimeStep))

	return st, nil
}

// Returns a time.Time representing the current timestamp, based on the last message published in the `time` stream,
// or defaultTime if the stream is empty.
// Only returns an error if the stream cannot be read at all, or when there's no valid timestamp in the stream's last
// message.
func (st *SimTimer) getCurrentTimeFromStream(defaultTime time.Time) (time.Time, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	log := st.logger.With("stream", NatsTimeStreamName, "subject", NatsTimeSubject)

	strm, err := st.js.Stream(ctx, NatsTimeStreamName)
	if err != nil {
		log.Error("failed to find stream", "err", err)
		return defaultTime, fmt.Errorf("failed to find stream: %w", err)
	}

	msg, err := strm.GetLastMsgForSubject(ctx, NatsTimeSubject)
	if err != nil {
		if errors.Is(err, jetstream.ErrMsgNotFound) {
			log.Info("no messages in stream, using default", "default", defaultTime)
			return defaultTime, nil
		}

		log.Debug("failed to get last message", "err", err)

		return defaultTime, fmt.Errorf("failed to get last message: %w", err)
	}

	log.Debug("last message in stream found", "nats-msg", string(msg.Data))

	return time.Parse(TimeFormat, string(msg.Data))
}

func (st *SimTimer) Run(ctx context.Context) error {
	if err := st.run(ctx); err != nil {
		return err
	}

	st.state = StateStopped

	return nil
}

func (st *SimTimer) run(ctx context.Context) error {
	st.state = StateRunning

	log := st.logger.
		With("consumerLagTolerance", st.ConsumerLagTolerance,
			"consumerNames", strings.Join(st.ConsumerNames, ","),
			"endTime", st.EndTime,
			"startTime", st.StartTime,
			"currentTime", st.CurrentTime.Load().(time.Time),
			"timeStep", st.TimeStep.String(),
		)

	log.Info("SimTimer started")

	currentTime := st.CurrentTime.Load().(time.Time)
	for currentTime.Before(st.EndTime) {
		select {
		case <-ctx.Done():
			log.Info("SimTimer canceled")
			return nil
		case <-st.stop:
			log.Info("SimTimer stopped")
			return nil
		default:
			if err := st.waitForAllConsumers(ctx); err != nil {
				if !errors.Is(err, context.Canceled) {
					log.Error("failed to wait for all consumers to ack", "err", err)
				}

				return err
			}

			if err := st.publishTime(ctx, currentTime); err != nil {
				if !errors.Is(err, context.Canceled) {
					log.Error("failed to publish time", "err", err)
				}

				return err
			}

			currentTime = currentTime.Add(st.TimeStep)
			st.CurrentTime.Store(currentTime)
			st.Count.Add(1)
		}
	}

	return nil
}

func (st *SimTimer) IsRunning(ctx context.Context) bool {
	return st.state == StateRunning
}

func (st *SimTimer) Stop(ctx context.Context) {
	st.logger.Debug("Stopping simtimer")

	if st.state == StateRunning {
		st.stop <- true
	}

	st.logger.Debug("Stopped simtimer")
}

type Start struct {
	StartTime time.Time
	EndTime   time.Time
}

func (st *SimTimer) Start(ctx context.Context, start Start) {
	st.StartTime = start.StartTime
	st.EndTime = start.EndTime
	st.CurrentTime.Store(st.StartTime)

	if err := st.Run(ctx); err != nil {
		log := st.logger.With("cnt", st.Count.Load())
		if errors.Is(err, context.Canceled) {
			log.Warn("SimTimer run canceled")
			return
		}

		log.Error("SimTimer run ended prematurely", "err", err)
		return
	}

	st.logger.Info("simtimer run ended", "cnt", st.Count.Load())
}

func (st *SimTimer) ClearTimeStream(ctx context.Context) error {
	if err := st.clearStream(ctx, NatsTimeStreamName); err != nil {
		return fmt.Errorf("clear stream: %w", err)
	}

	st.logger.Info("time stream emptied")

	return nil
}

func (st *SimTimer) ClearEventsStream(ctx context.Context) error {
	if err := st.clearStream(ctx, NatsEventsStreamName); err != nil {
		return fmt.Errorf("clear stream: %w", err)
	}

	st.logger.Info("events stream emptied")

	return nil
}

func (st *SimTimer) clearStream(ctx context.Context, streamName string) error {
	stream, err := st.js.Stream(ctx, streamName)
	if err != nil {
		return fmt.Errorf("stream '%s' failed: %w", streamName, err)
	}

	if err := stream.Purge(ctx); err != nil {
		return fmt.Errorf("stream '%s' purge failed: %w", streamName, err)
	}

	return nil
}

func (st *SimTimer) waitForAllConsumers(ctx context.Context) error {
	wait := true
	cnt := 0

	// Needs an improvement, currently this can hard block the simtimer and thus this thread
	for {
		wait = false

		for _, consName := range st.ConsumerNames {
			log := st.logger.With("consumer-name", consName)

			cons, err := st.js.Consumer(ctx, NatsTimeStreamName, consName)
			if err != nil {
				if errors.Is(err, jetstream.ErrConsumerNotFound) {
					log.Debug("waiting for consumer to exists")
					wait = true
					break
				} else if !errors.Is(err, context.Canceled) {
					log.Error("failed to get consumer", "err", err)
				}

				return err
			}

			consInfo, err := cons.Info(ctx)
			if err != nil {
				log.Error("failed to get info of consumer", "err", err)
				return err
			}

			if consInfo.NumPending > uint64(st.ConsumerLagTolerance) {
				log.
					With("pending", consInfo.NumPending, "tolerance", st.ConsumerLagTolerance).
					Debug("Waiting for consumer to catch up")

				wait = true
				break
			}
		}

		if !wait {
			st.logger.Debug("Consumers are catched up")
			break
		}

		cnt = (cnt + 1) % ConsumerWaitLogCounter
		if cnt == 0 {
			st.logger.Warn("Waiting for all consumers to catch up")
		}

		time.Sleep(ConsumerWaitDelay)
	}

	return nil
}

func (st *SimTimer) PublishReset(ctx context.Context) error {
	return st.publishCommand(ctx, "reset")
}

func (st *SimTimer) PublishStart(ctx context.Context) error {
	return st.publishCommand(ctx, "start")
}

func (st *SimTimer) publishTime(ctx context.Context, t time.Time) error {
	return st.publish(ctx, NatsTimeSubject, makeTimeMessage(t))
}

func (st *SimTimer) publishCommand(ctx context.Context, command string) error {
	return st.publish(ctx, NatsCommandSubject, command)
}

func (st *SimTimer) publish(ctx context.Context, subject string, msg string) error {
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	ack, err := st.js.Publish(ctx, subject, []byte(msg))
	if err != nil {
		return fmt.Errorf("publish failed: %w", err)
	}

	st.logger.
		With("natsAckSeq", ack.Sequence, "natsMsg", msg).
		Debug("JetStream message published")

	return nil
}

func makeTimeMessage(t time.Time) string {
	return t.Format(TimeFormat)
}
