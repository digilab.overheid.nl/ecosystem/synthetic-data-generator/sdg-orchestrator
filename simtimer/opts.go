package simtimer

import "time"

type Option func(*SimTimer)

func WithStartTime(t time.Time) Option {
	return func(st *SimTimer) {
		st.StartTime = t
	}
}

func WithEndTime(t time.Time) Option {
	return func(st *SimTimer) {
		st.EndTime = t
	}
}

func WithTimeStep(d time.Duration) Option {
	return func(st *SimTimer) {
		st.TimeStep = d
	}
}

func WithConsumerLagTolerance(t int) Option {
	return func(st *SimTimer) {
		st.ConsumerLagTolerance = t
	}
}

func WithConsumerNames(cn []string) Option {
	return func(st *SimTimer) {
		st.ConsumerNames = cn
	}
}
