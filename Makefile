GO_CMD := go
BUILD_DIR := ./build
BINARY_NAME := sdg-orchestrator
BINARY_PATH := $(BUILD_DIR)/$(BINARY_NAME)
NATS_HOST := localhost
NATS_PORT := 4222

.PHONY: all
all: clean build test

.PHONY: build
build:
	"$(GO_CMD)" build -o "$(BINARY_PATH)"

.PHONY: clean
clean:
	"$(GO_CMD)" clean
	rm -r "$(BUILD_DIR)" || true

.PHONY: dev
dev:
	APP_DEBUG= \
	SDG_NATS_SERVICE_HOST=$(NATS_HOST) \
	SDG_NATS_SERVICE_PORT=$(NATS_PORT) \
	SDG_SIMULATION_START=1990-01-01T00:00:00.00Z \
	SDG_SIMULATION_END=1991-01-01T00:00:00.00Z \
	SDG_SIMULATION_TIME_STEP=6h \
	SDG_SIMULATION_TIME_CONSUMER_NAMES=sdg-event-simulator-person \
	SERVER_PORT=8082 \
	go run *.go

.PHONY: prod
prod: clean build
	SDG_NATS_SERVICE_HOST=$(NATS_HOST) \
	SDG_NATS_SERVICE_PORT=$(NATS_PORT) \
	SDG_SIMULATION_START=1990-01-01T00:00:00.00Z \
	SDG_SIMULATION_END=1991-01-01T00:00:00.00Z \
	SDG_SIMULATION_TIME_STEP=6h \
	SDG_SIMULATION_TIME_CONSUMER_NAMES=sdg-event-simulator-person \
	SERVER_PORT=8082 \
	"$(BINARY_PATH)"

.PHONY: test
test:
	go test ./...

.PHONY: help
help:
	@echo "Available targets:"
	@echo "  all       : Clean, build and test"
	@echo "  build     : Build the application"
	@echo "  clean     : Clean up artifacts"
	@echo "  dev       : Build and run the application in dev-mode"
	@echo "  prod      : Build and run the application in prod-mode"
	@echo "  test      : Run automated tests"
